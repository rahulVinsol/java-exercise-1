import java.util.Calendar;

public class Main {

	public static void main(String[] args) {
		MovieDAO movieDao=new MovieDAO();
		movieDao.addMovie(new Movie("Prisoner of Azkaban",2004,"English","Fictional",Calendar.getInstance().getTime(),MovieType.HollyWood));
		movieDao.addMovie(new Movie("Goblet of Fire",2005,"English","Fictional",Calendar.getInstance().getTime(),MovieType.HollyWood));
		movieDao.addMovie(new Movie("Sachin",2017,"Hindi","Documentary",Calendar.getInstance().getTime(),MovieType.BollyWood));
		movieDao.addMovie(new Movie("Bahubali",2015,"Tamil","Fictional",Calendar.getInstance().getTime(),MovieType.TollyWood));

		System.out.println("All Movies: "+movieDao.getAllMovies());
		System.out.println("\nHollyWood Movies: "+movieDao.getAllMoviesOfType(MovieType.HollyWood));

		FoodStoreDAO foodStoreDao=new FoodStoreDAO();
		System.out.println("\nHollyWood Free Dish: "+foodStoreDao.getFreeDish(MovieType.HollyWood));

		Movie movie=movieDao.getMovieFromName("Sachin");
		System.out.println("\nMovie Name: "+movie);
		for(int i=0;i<5&&movie!=null;i++)
			System.out.println("Was "+movie.getName()+" Blockbuster: "+movie.isBlockbuster());

		movie=movieDao.getMovieFromName("Goblet of Fire");
		System.out.println("\nMovie Name: "+movie);
		for(int i=0;i<5&&movie!=null;i++)
			System.out.println("Was "+movie.getName()+" Blockbuster: "+movie.isBlockbuster());
	}
}

