import java.util.ArrayList;
import java.util.List;

public class MovieDAO {
	/**
	 * Global movie list that will hold all the Movies
	 */
	private List<Movie> movies;

	/**
	 * Adds a specified Movie to the global movie list
	 *
	 * @param movie Movie to be added
	 */
	public void addMovie(Movie movie) {
		if (movies == null)
			movies = new ArrayList<Movie>();
		movies.add(movie);
	}

	/**
	 * Deletes a specified Movie from global movie list
	 *
	 * @param movie Movie to be deleted
	 */
	public void deleteMovie(Movie movie) {
		deleteMovie(movies.indexOf(movie));
	}

	/**
	 * Deletes a Movie at a specified index in global list
	 *
	 * @param i index of Movie to be deleted
	 */
	public void deleteMovie(int i) {
		if(movies==null)
			return;
		deleteMovie(i);
	}

	/**
	 * Retrieve all Movies that were added
	 *
	 * @return List of Movies
	 */
	public List<Movie> getAllMovies() {
		if (movies == null)
			movies = new ArrayList<>();
		return movies;
	}

	/**
	 * Tells whether a specified Movie was blockbuster or not
	 *
	 * @param movie Movie for blockbuster query
	 * @return true/false depending on whether Movie was blockbuster or not
	 */
	public boolean isMovieBlockbuster(Movie movie) {
		return movie.isBlockbuster();
	}

	/**
	 * Retrieve list of Movies that belong to a specified MovieType
	 *
	 * @param movieType MovieType whose Movies are to be retrieved
	 * @return List of Movies
	 */
	public List<Movie> getAllMoviesOfType(MovieType movieType) {
		ArrayList<Movie> localMovies = new ArrayList<>();
		for (Movie movie : movies) {
			if (movie.getMovieType() == movieType) {
				localMovies.add(movie);
			}
		}
		return localMovies;
	}

	/**
	 * Retrieve Movie based on it's name from ArrayList
	 * 
	 * @param name The Name of Movie to be found
	 * @return Movie based on it's Name
	 */
	public Movie getMovieFromName(String name) {
		if(name==null)
			return null;	
		for(Movie movie:movies) {
			if(movie.getName().equals(name)) {
				return movie;
			}
		}
		return null;
	}
}
