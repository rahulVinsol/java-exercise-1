/**
 * Enum for Types of Movies
 */
public enum MovieType {
	TollyWood(1_000),
	BollyWood(1_00_000),
	HollyWood(1_000_000);

	/**
	 * Cost of production for the Movie
	 */
	private double costOfProduction;

	/**
	 * constructor for enum MovieType
	 *  
	 * @param costOfProduction Cost of Movie Production
	 */
	MovieType(double costOfProduction) {
		this.costOfProduction=costOfProduction;
	}

	/**
     * Retrieve Cost of Production
     *
     * @return cost of Production
     */
	public double getCostOfProduction() {
		return costOfProduction;
	}
}