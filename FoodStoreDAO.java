import java.util.HashMap;
import java.util.Map;

public class FoodStoreDAO {
	/**
	 * Map that maps one Food Store to a Movie Type
	 */
	private Map<MovieType, FoodStore> foodStoreMap;
	
	/**
	 * Constructor for class FoodStoreDAO
	 */
	public FoodStoreDAO() {
		this.foodStoreMap = new HashMap<>();
	}

	/**
	 * Serves free dish depending on Movie Type
	 * 
	 * @param movieType the Movie Type whose Free Dish is required
	 * @return Free Dish depending on the Movie Type
	 */
	public FoodStore.FreeDish getFreeDish(MovieType movieType){
		if(foodStoreMap.get(movieType)==null) {
			foodStoreMap.put(movieType, new FoodStore(movieType));
		}
		return foodStoreMap.get(movieType).getFreeDish();
	}
}