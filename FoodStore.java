public class FoodStore {
	/**
	 * Enum for Types of Free Dishes
	 */
	public enum FreeDish {
		Dosa,
		DalMakhni,
		PepperSteak
	}

	/**
	 * Type of movie this Food store belongs to
	 */
	private MovieType movieType;

	/**
	 * Free dish that this Food Store Serves
	 */
	private FreeDish freeDish;

	/**
	 * Constructor for FoodStore
	 *
	 * @param movieType type of Movie this FoodStore belongs to
	 */
	public FoodStore(MovieType movieType) {
		this.movieType=movieType;
		switch (movieType) {
			case TollyWood:
				freeDish = FoodStore.FreeDish.Dosa;
				break;
			case BollyWood:
				freeDish = FoodStore.FreeDish.DalMakhni;
				break;
			case HollyWood:
				freeDish = FoodStore.FreeDish.PepperSteak;
				break;
		}
	}

	/**
	 * Gets the Free Dish that this FoodStore offers
	 *
	 * @return Free Dish that this FoodStore offers
	 */
	public FreeDish getFreeDish() {
		return freeDish;
	}

	/**
	 * Gets the Movie Type this Food Store belongs to
	 * 
	 * @return Movie Type this FoodS Store belongs to
	 */
	public MovieType getMovieType() {
		return movieType;
	}
}

