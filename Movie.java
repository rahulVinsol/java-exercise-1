import java.util.Date;
import java.util.Random;

public class Movie {
	/**
	 * Constructor for class Movie
	 *
	 * @param name Name of Movie
	 * @param year Year of Release of the Movie
	 * @param language Language the Movie was created in
	 * @param genre Genre the Movie belongs to
	 * @param releaseDate Release date of said Movie
	 * @param movieType Type of Movie
	 */
	public Movie(String name,int year,String language,String genre,Date releaseDate,MovieType movieType) {
		this.name=name;
		this.year=year;
		this.language=language;
		this.genre=genre;
		this.releaseDate=releaseDate;
		this.movieType = movieType;
	}

	/**
	 * Type of Movie i.e. TollyWood, BollyWood or HollyWood
	 */
	private MovieType movieType;

	/**
	 * Name of the Movie
	 */
	private String name;

	/**
	 * Year in which Movie was released
	 */
	private int year;

	/**
	 * Language of the Movie
	 */
	private String language;

	/**
	 * Genre the Movie belongs to
	 */
	private String genre;

	/**
	 * Release Date of Movie
	 */
	private Date releaseDate;

	/**
	 * Is the Movie a Blockbuster Hit
	 */
	private Boolean isBlockbuster;

	/**
	 * Retrieve Type of Movie
	 *
	 * @return type of Movie
	 */
	public MovieType getMovieType() {
		return movieType;
	}

	/**
	 * Set Type of Movie
	 *
	 * @param movieType type of Movie
	 */
	public void setMovieType(MovieType movieType) {
		this.movieType = movieType;
	}

	/**
	 * Retrieve Name of Movie
	 *
	 * @return Name of Movie
	 */
	public String getName() {
		return name;
	}

	/**
	 * Set Name of Movie
	 *
	 * @param name Name of Movie
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Get Release Year of Movie
	 *
	 * @return Release Year of Movie
	 */
	public int getYear() {
		return year;
	}

	/**
	 * Set Release Year of Movie
	 *
	 * @param year Release Year of Movie
	 */
	public void setYear(int year) {
		this.year = year;
	}

	/**
	 * Retrieve Language of Movie
	 *
	 * @return Language of Movie
	 */
	public String getLanguage() {
		return language;
	}

	/**
	 * Set Language of Movie
	 *
	 * @param language Language of Movie
	 */
	public void setLanguage(String language) {
		this.language = language;
	}

	/**
	 * Retrieve Genre of Movie
	 *
	 * @return Genre of Movie
	 */
	public String getGenre() {
		return genre;
	}

	/**
	 * Set Genre of Movie
	 *
	 * @param genre Genre of Movie
	 */
	public void setGenre(String genre) {
		this.genre = genre;
	}

	/**
	 * Retrieve Release Date of Movie
	 *
	 * @return Release Date of Movie
	 */
	public Date getReleaseDate() {
		return releaseDate;
	}

	/**
	 * Set Release Date of Movie
	 *
	 * @param releaseDate Release Date of Movie
	 */
	public void setReleaseDate(Date releaseDate) {
		this.releaseDate = releaseDate;
	}

	/**
	 * Retrieve if the Movie was a Blockbuster Hit
	 *
	 * @return Whether the Movie was Blockbuster
	 */
	public Boolean isBlockbuster() {
		if (isBlockbuster == null) {
			isBlockbuster = new Random().nextFloat() > 0.5f;
		}
		return isBlockbuster;
	}

	@Override
	public String toString() {
		return name;
	}
}

